#### REQUIREMENTS
## TensorFlow, tf.keras, helper libraries
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model, load_model
from keras.layers import Activation, Dropout, Flatten, Dense, Conv2D, MaxPooling2D
from keras.applications import densenet
from keras.applications.resnet50 import decode_predictions
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, Callback
from keras import regularizers
from keras import backend as K
from sklearn.metrics import confusion_matrix, classification_report

import os
import numpy as np
import random
import cv2
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt

from pathlib import Path
from scipy.io import loadmat


#### DATA
# Data paths (local)
cars_train_path = Path('train')
cars_test_path = Path('test/test')
dev_kit_path = Path('devkit')

# Load mat data for annotations
cars_meta = loadmat(dev_kit_path / 'cars_meta.mat')
cars_train_annos = loadmat(dev_kit_path / 'cars_train_annos.mat')
cars_test_annos = loadmat(dev_kit_path / 'cars_test_annos.mat')

# Initiation of global variables
train_num_samples = 0
train_num = 0
valid_num = 0
test_num = 0
test_num_samples = 0
train_ids = 0

### TRAIN

## Prepare + Process
# split of 0.75 (train) / 0.25 (valid)
# with labels
# with ids
# cropped, resized, bboxed
def prep_train_data(train_file_images, train_labels, train_bboxes):
    global train_num_samples
    train_num_samples = len(train_file_images)

    train_split = 0.80
    global train_num
    train_num = int(((train_num_samples * train_split)))
    train_indexes = random.sample(range(train_num_samples), train_num)
    
    global valid_num
    valid_num = int(round(train_num_samples * (1-train_split)))

    for c in range(train_num_samples):
        train_file_image_name = train_file_images[c]
        train_label = train_labels[c]
        (x1, y1, x2, y2) = train_bboxes[c]

        raw_train_image_path = os.path.join(cars_train_path, train_file_image_name)
        raw_train_image = cv2.imread(raw_train_image_path)
        train_height, train_width = raw_train_image.shape[:2]
        # margins of 12 pixels
        margin = 12
        x1 = max(0, x1 - margin)
        y1 = max(0, y1 - margin)
        x2 = min(x2 + margin, train_width)
        y2 = min(y2 + margin, train_height)

        if c in train_indexes:
            save_folder = 'data/train'
        else:
            save_folder = 'data/valid'

        save_image_path = os.path.join(save_folder, train_label)
        if not os.path.exists(save_image_path):
            os.makedirs(save_image_path)
        save_image_path = os.path.join(save_image_path, train_file_image_name)

        train_image_cropped = raw_train_image[y1:y2, x1:x2]
        train_image_resized = cv2.resize(src=train_image_cropped, dsize=(train_height, train_width))
        cv2.imwrite(save_image_path, train_image_resized)
        

def proc_train_data():
    train_file_images = []
    train_labels = []
    train_bboxes = []
    global train_ids
    train_ids = []

    for c in cars_train_annos['annotations'][0]:
        train_file_image_name = c[5][0]
        train_id = c[4][0][0]
        bbox_x1 = c[0][0][0]
        bbox_y1 = c[1][0][0]
        bbox_x2 = c[2][0][0]
        bbox_y2 = c[3][0][0]
        train_file_images.append(train_file_image_name)
        train_labels.append('%04d' % (train_id,))
        train_bboxes.append((bbox_x1, bbox_y1, bbox_x2, bbox_y2))

    prep_train_data(train_file_images, train_labels, train_bboxes)


proc_train_data()


#### MODEL

### Parameters for the model
K.set_learning_phase(1)
images_width = 224
images_height = 224
train_data = 'data/train'
valid_data = 'data/valid'
test_data = 'data/test'
classes_names = cars_meta['class_names'][0]
train_ids = train_ids
num_classes = 196
num_train_samples = 6515
num_valid_samples = 1629
num_test_samples = 8041
batch_size = 64
num_epochs = 5
validation_steps = num_valid_samples // batch_size
test_steps = num_test_samples // batch_size

# BUILD the model 
train_datagen = ImageDataGenerator(
    rescale = 1./255,
    shear_range = 0.2,
    zoom_range = 0.2,
    horizontal_flip = True)

valid_datagen = ImageDataGenerator(rescale=1./255)

valid_generator.reset()

train_generator = train_datagen.flow_from_directory(
    'data/train',
    target_size = (images_width, images_height),
    batch_size = batch_size,
    class_mode = 'categorical')

valid_generator = valid_datagen.flow_from_directory(
    'data/valid',
    target_size = (images_width, images_height),
    batch_size = 1)

def build_model():
    base_model = densenet.DenseNet121(
        input_shape=(images_width, images_height, 3),
        weights='imagenet',
        include_top=False,
        pooling='avg')
    for layer in base_model.layers:
      layer.trainable = True

    x = base_model.output
    x = Dense(392, kernel_regularizer=regularizers.l1_l2(0.01), activity_regularizer=regularizers.l2(0.01))(x)
    x = Activation('relu')(x)
    predictions = Dense(num_classes, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)
    
    return model

model = build_model()

model.summary()

model.compile(optimizer='adam', 
              loss='categorical_crossentropy',
              metrics=['accuracy', 'mse'])

early_stop = EarlyStopping(monitor='val_loss',
                            patience=8,
                            verbose=1,
                            min_delta=1e-4)

reduce_lrp = ReduceLROnPlateau(monitor='val_loss',
                            factor=0.1,
                            patience=4,
                            verbose=1,
                            min_delta=1e-4)

callbacks_list = [early_stop, reduce_lrp]


# TRAIN the model
model_in_training = model.fit_generator(
    train_generator,
    epochs = num_epochs,
    steps_per_epoch = num_train_samples // batch_size,
    validation_data = valid_generator,
    validation_steps = num_valid_samples // batch_size,
    callbacks = callbacks_list,
    shuffle = False)

# EVALUATE the model
score_valid = model.evaluate_generator(valid_generator,
                                        num_valid_samples)

print('Loss Evaluate = ', score_valid[0])
print('Accuracy Evaluate = ', score_valid[1])


# PREDICT with the model
predict_valid = model.predict_generator(valid_generator,
                                    verbose = 1,
                                    steps = num_valid_samples // 1)

predicted_classes_valid = np.argmax(predict_valid, axis=1)

print('Confusion Matrix')
cm = confusion_matrix(valid_generator.classes, predicted_classes_valid)
plt.figure(figsize = (30, 20))
sn.set(font_scale=1.4)
sn.heatmap(cm, annot=True, annot_kws={"size": 14})
plt.savefig('confusion_matrix.png')
plt.show()